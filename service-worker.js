

const cacheName = "nosad";

const addResourcesToCache = async (resources) => {
  const cache = await caches.open(cacheName);
  await cache.addAll(resources);
};

// Enable navigation preload
const enableNavigationPreload = async () => {
  if (self.registration.navigationPreload) {
    // Enable navigation preloads!
    await self.registration.navigationPreload.enable();
  }
};

async function fetchAndCacheIfOk(event) {
  try {
    const response = await fetch(event.request);

    // don't cache non-ok responses
    if (response.ok) {
      const responseClone = response.clone();
      const cache = await caches.open(cacheName);
      //await cache.put(event.request, responseClone);

      var headers = new Headers(responseClone.headers);
      headers.append('sw-fetched-on', new Date().getTime());

      responseClone.blob().then(function (body) {
        cache.put(event.request, new Response(body, {
          status: responseClone.status,
          statusText: responseClone.statusText,
          headers: headers
        }));
      });

    }

    return response;
  } catch (e) {
    const cache = await caches.open(cacheName);
    const response = await cache.match(event.request);
    if (!!response) {
      console.log("Network is down, maybe you are on a fligt, the cache is not updated, serve something");
      return response;
    }
    return e;
  }
}

var isValid = function (response) {
  if (!response) return false;
  var fetched = response.headers.get('sw-fetched-on');
  if (fetched && (parseFloat(fetched) + (1000 * 60 * 60 * 2)) > new Date().getTime()) return true;
  return false;
};

async function fetchWithCache(event) {
  const cache = await caches.open(cacheName);
  const response = await cache.match(event.request);
  const withinTimeSlot = isValid(response);
  if (!!response && withinTimeSlot) {
    // it is cached but we want to update it so request but not await
    fetchAndCacheIfOk(event);
    // return the cached response
    return response;
  } else {
    // it was not cached yet so request and cache
    return fetchAndCacheIfOk(event);
  }
}

function handleFetch(event) {
  // only intercept the request if there is no no-cache header
  if (event.request.headers.get("cache-control") !== "no-cache") {
    // important: respondWith has to be called sync, otherwise
    // the service worker won't know whats going on.
    // Had to learn this the hard way
    event.respondWith(fetchWithCache(event));
  }
}

self.addEventListener('install', (event) => {
  event.waitUntil(
    addResourcesToCache([
      '/internationellt',
      '/tips',
      '/workshops',
      '/organisationer',
      '/katalog',
    ])
  );
});

self.addEventListener("fetch", handleFetch);
